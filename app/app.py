# import the Flask class from the flask module
from flask import Flask, render_template
from flask import request
import base64
from PIL import Image, ImageChops, ImageEnhance, ImageOps
from PIL import UnidentifiedImageError
import PIL
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy import Boolean, Column, Date, Integer, Text, create_engine, inspect 
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.orm import sessionmaker 
from sqlalchemy.ext.declarative import declarative_base
import time
import operationsbd

# create the application object
app = Flask(__name__)

# use decorators to link the function to a url
@app.route('/', methods=['GET'])
def home():

    return render_template('index.html')
    #return "Hello, World!"  # return a string

@app.route('/deletedata/', methods=['GET', 'POST'])
def deletedata():
    res = ''
    if request.method == "POST":
        #Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
        check_bd = operationsbd.OperationsBD()

        #Se invoca la función proveniente de la clase OperationsBD
        res = check_bd.deleteData()

        if res:
            return "Se ha reiniciado la tabla de las estadisticas de la API"
        else:
            return "Por favor, verifique la conexión al servidor de base de datos, que el mismo se encuentre activo."
    else:
        return "Solo se aceptan solicitudes POST via Ajax por el momento."

    

@app.route('/getdata/', methods=['GET', 'POST'])
def getdata():
    #Cronometro sencillo
    start_time = time.time()
    cantidad_registros = 0

    if request.method == "POST":
        #Verificamos el filtro que se selecciono y se aplica el filtro adecuado.
        search_term = request.form
        if search_term:
            print("Cantidad a obtener: "+str(search_term["regcantidad"]))
            cantidad_registros = search_term["regcantidad"]
        else:
            print("Cantidad por defecto: 10")
            cantidad_registros = 10

        tiempo_total = 0
        cantidad_archivos = 0
        cantidad_operaciones_completadas = 0
        cantidad_operaciones_incompletas = 0

        #Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
        check_bd = operationsbd.OperationsBD()
        #Se crean las tablas previamente declaradas.
        check_bd.create_tables()

        #html a renderizar en template.
        html_data = """<table id='results_table' border = '1'>
                            <thead>
                                <th width=100>Nombre</th>
                                <th width=100>Operación</th>
                                <th>Tiempo</th>
                                <th>Completado</th>
                                <th>Log</th>
                            </thead>
                    """

        #Se invoca la función proveniente de la clase OperationsBD
        data = check_bd.getData()

        if cantidad_registros:
            cantidad_registros = int(cantidad_registros)
            if cantidad_registros > 0:
                for row in data: 
                    fila = row._asdict()
                    tiempo_total = tiempo_total + float(fila['time'])
                    cantidad_archivos += 1

                    if fila['success']:
                        cantidad_operaciones_completadas += 1
                    else:
                        cantidad_operaciones_incompletas += 1

                    html_data = html_data + """<tr>
                                                <td><div style="overflow:auto; width:100px">"""+str(fila['name']) + """</div></td>
                                                <td><div style="overflow:auto; width:100px">"""+str(fila['operation']) + """</div></td>
                                                <td><div style="overflow:auto; width:100px">"""+str(fila['time']) + """</div></td>
                                                <td><div style="overflow:auto; width:100px">"""+str(fila['success']) + """</div></td>
                                                <td><div style="overflow:auto;">"""+str(fila['log']) + """</div></td>
                                               </tr>
                                            """
                    #Para visualizar una cantidad determinada de registros.
                    cantidad_registros -= 1
                    if cantidad_registros < 1:
                        break
        
        html_data = html_data + """
                                    <tr>
                                        <td colspan = 5>
                                            Estadisticas varias
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan = 5>
                                            Total tiempo de ejecución: """ + str(tiempo_total) + """
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan = 5>
                                            Cantidad de archivos analizados: """ + str(cantidad_archivos) + """
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan = 5>
                                            Operaciones exitosas: """ + str(cantidad_operaciones_completadas) + """
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan = 5>
                                            Operaciones fallidas: """ + str(cantidad_operaciones_incompletas) + """
                                        </td>
                                    </tr>
                                """

        html_data = html_data + "</table>"

        end_time = time.time()

        #Se obtiene un aproximado del tiempo de ejecución de la función antes de renderizar.
        total_time = end_time - start_time

        return render_template("welcome.html", operation="2",
                                               total_time=total_time,
                                               data=html_data)
    else:
        return "Solo se aceptan solicitudes POST via Ajax por el momento."

@app.route('/setfilter/', methods=['GET', 'POST'])         
def setfilter():
    #Cronometro sencillo
    start_time = time.time()

    if request.method == "POST":
        
        #Verificamos el filtro que se selecciono y se aplica el filtro adecuado.
        search_term = request.form
        file_original_str = ''
        file_final_str = ''
        #print("Filtro: "+str(search_term["procedure"]))

        if search_term:
            print("Procedimiento seleccionado: "+str(search_term["procedure"]))
        else:
            return "Debe indicar un procedimiento valido, así como enviar una imagen mediante Ajax usando la interfaz que provee la API."

        if str(search_term["procedure"]) == "invcolor":
            print("Se le invierte el color a la imagen.")

            # Verificar que se ha subido una imagen.
            if 'pictureFile' not in request.files:
                return "Debe seleccionar una imagen"
            file = request.files['pictureFile']
            file_original_str = base64.b64encode(file.read()).decode('UTF-8')

            # Invertir colores.
            try:
                image = Image.open(file)
                image = ImageChops.invert(image)
                file.seek(0)
                image.save(file, "jpeg")
                file.seek(0)
                file_final_str = base64.b64encode(file.read()).decode('UTF-8')
            except PIL.UnidentifiedImageError as e:
                #Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
                check_bd = operationsbd.OperationsBD()
                #Se crean las tablas previamente declaradas.
                check_bd.create_tables()

                end_time = time.time()

                #Se obtiene un aproximado del tiempo de ejecución de la función antes de renderizar.
                total_time = end_time - start_time

                #Se invoca la función proveniente de la clase OperationsBD
                check_bd.insertData(file.filename, "invcolor", total_time, False, "Error: "+str(e))
                return "Error al cargar imagen: "+str(e)
        
            print("Archivos: "+str(file.filename))

            #Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
            check_bd = operationsbd.OperationsBD()
            #Se crean las tablas previamente declaradas.
            check_bd.create_tables()

            end_time = time.time()

            #Se obtiene un aproximado del tiempo de ejecución de la función antes de renderizar.
            total_time = end_time - start_time

            #Se invoca la función proveniente de la clase OperationsBD
            check_bd.insertData(file.filename, "invcolor", total_time, True, "Log-inv-color")
            
            #print("File en base 64: "+str(file_original_str))
            return render_template("welcome.html", operation="1",
                                                   imagen_original=file_original_str,
                                                   imagen_final=file_final_str)

        elif str(search_term["procedure"]) == "blkandwth":
            print("Se aplica escala de grises a la imagen.")

            # Verificar que se ha subido una imagen.
            if 'pictureFile' not in request.files:
                return "Debe seleccionar una imagen"
            file = request.files['pictureFile']
            file_original_str = base64.b64encode(file.read()).decode('UTF-8')

            # Blanco y Negro.
            try:
                image = Image.open(file)
                image = ImageOps.grayscale(image)
                file.seek(0)
                image.save(file, "jpeg")
                file.seek(0)
                file_final_str = base64.b64encode(file.read()).decode('UTF-8')
            except PIL.UnidentifiedImageError as e:
                #Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
                check_bd = operationsbd.OperationsBD()
                #Se crean las tablas previamente declaradas.
                check_bd.create_tables()

                end_time = time.time()

                #Se obtiene un aproximado del tiempo de ejecución de la función antes de renderizar.
                total_time = end_time - start_time

                #Se invoca la función proveniente de la clase OperationsBD
                check_bd.insertData(file.filename, "blkandwth", total_time, False, "Error: "+str(e))
                return "Error al cargar imagen: "+str(e)

            print("Archivos: "+str(file.filename))
            
            #Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
            check_bd = operationsbd.OperationsBD()
            #Se crean las tablas previamente declaradas.
            check_bd.create_tables()

            end_time = time.time()

            #Se obtiene un aproximado del tiempo de ejecución de la función antes de renderizar.
            total_time = end_time - start_time
            
            #Se invoca la función proveniente de la clase OperationsBD
            check_bd.insertData(file.filename, "blkandwth", total_time, True, "Log-blkandwth")

            return render_template("welcome.html", operation="1",
                                                   imagen_original=file_original_str,
                                                   imagen_final=file_final_str)

        elif str(search_term["procedure"]) == "rotateimg":
            print("Se aplica rotación de 90º a la imagen.")

            # Verificar que se ha subido una imagen.
            if 'pictureFile' not in request.files:
                return "Debe seleccionar una imagen"
            file = request.files['pictureFile']
            file_original_str = base64.b64encode(file.read()).decode('UTF-8')

            # Rotar 90º.
            try:
                image = Image.open(file)
                image = image.rotate(90)
                file.seek(0)
                image.save(file, "jpeg")
                file.seek(0)
                file_final_str = base64.b64encode(file.read()).decode('UTF-8')
            except PIL.UnidentifiedImageError as e:
                #Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
                check_bd = operationsbd.OperationsBD()
                #Se crean las tablas previamente declaradas.
                check_bd.create_tables()

                end_time = time.time()

                #Se obtiene un aproximado del tiempo de ejecución de la función antes de renderizar.
                total_time = end_time - start_time

                #Se invoca la función proveniente de la clase OperationsBD
                check_bd.insertData(file.filename, "rotateimg", total_time, False, "Error: "+str(e))
                return "Error al cargar imagen: "+str(e)

            print("Archivos: "+str(file.filename))
            
            #Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
            check_bd = operationsbd.OperationsBD()
            #Se crean las tablas previamente declaradas.
            check_bd.create_tables()

            end_time = time.time()

            #Se obtiene un aproximado del tiempo de ejecución de la función antes de renderizar.
            total_time = end_time - start_time
            
            #Se invoca la función proveniente de la clase OperationsBD
            check_bd.insertData(file.filename, "rotateimg", total_time, True, "Log-rotateimg")

            return render_template("welcome.html", operation="1",
                                                   imagen_original=file_original_str,
                                                   imagen_final=file_final_str)
        
        elif str(search_term["procedure"]) == "invyaxis":
            print("Se aplica inversión de eje Y a la imagen.")

            # Verificar que se ha subido una imagen.
            if 'pictureFile' not in request.files:
                return "Debe seleccionar una imagen"
            file = request.files['pictureFile']
            file_original_str = base64.b64encode(file.read()).decode('UTF-8')

            # Invertir imagen sobre eje Y.
            try:
                image = Image.open(file)
                image = ImageOps.flip(image)
                file.seek(0)
                image.save(file, "jpeg")
                file.seek(0)
                file_final_str = base64.b64encode(file.read()).decode('UTF-8')
            except PIL.UnidentifiedImageError as e:
                #Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
                check_bd = operationsbd.OperationsBD()
                #Se crean las tablas previamente declaradas.
                check_bd.create_tables()

                end_time = time.time()

                #Se obtiene un aproximado del tiempo de ejecución de la función antes de renderizar.
                total_time = end_time - start_time

                #Se invoca la función proveniente de la clase OperationsBD
                check_bd.insertData(file.filename, "invyaxis", total_time, False, "Error: "+str(e))
                return "Error al cargar imagen: "+str(e)

            print("Archivos: "+str(file.filename))
            
            #Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
            check_bd = operationsbd.OperationsBD()
            #Se crean las tablas previamente declaradas.
            check_bd.create_tables()

            end_time = time.time()

            #Se obtiene un aproximado del tiempo de ejecución de la función antes de renderizar.
            total_time = end_time - start_time
            
            #Se invoca la función proveniente de la clase OperationsBD
            check_bd.insertData(file.filename, "invyaxis", total_time, True, "Log-invyaxis")

            return render_template("welcome.html", operation="1",
                                                   imagen_original=file_original_str,
                                                   imagen_final=file_final_str)
            return ''
    else:
        return "Solo se aceptan solicitudes POST via Ajax por el momento."

# start the server with the 'run()' method
if __name__ == '__main__':
    app.run(debug=True)
