from flask import Flask, render_template
from flask import request
import base64
from PIL import Image, ImageChops, ImageEnhance, ImageOps
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import text as sa_text
from flask_migrate import Migrate
from sqlalchemy import Boolean, Column, Date, Integer, Text, create_engine, inspect 
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.orm import sessionmaker 
from sqlalchemy.ext.declarative import declarative_base 
import timeit

#Se define una clase que va a chequear si existe o no la base de datos con la que trabaja la API
#de no existir entonces la crea, por defecto usa el Usuario postgres, este se puede cambiar
#segun se necesite.
class OperationsBD:

    def __init__(self):
        
        #Creando la conexión hacia la base de datos en entorno local.
        self.engine = create_engine("postgresql://postgres:root@localhost:5432/imagedatabd")
        if not database_exists(self.engine.url):
            #Si no existe la base de datos se crea una nueva.
            create_database(self.engine.url)
        
        #Objetos declarativos y de sesión para ejecutar sentencias y crear tablas.
        self.Base = declarative_base() 
        self.Session = sessionmaker(bind=self.engine)

        print("Status DB: "+str(database_exists(self.engine.url)))

    #Se ejecuta justo despues de creada o chequeada la base de datos, en caso de no existir las tablas
    #se crean unas nuevas, esto siguiendo los modelos declarados que se hayan creado con el
    #objeto Base como atributo (Ejemplo: ImgTransformModel)
    def create_tables(self):
        print("Chequeando las tablas!")
        self.Base.metadata.create_all(bind=self.engine) 
        self.Session.configure(bind=self.engine) 

    def insertData(self, name, operation, time, success, log):
        session = self.Session()
        session.add(ImgTransformModel(name=name, operation=operation, time=time, success=success, log=log)) 
        session.commit()

    def getData(self):
        session = self.Session()
        query = session.query(ImgTransformModel.name,
                              ImgTransformModel.operation,
                              ImgTransformModel.time,
                              ImgTransformModel.success,
                              ImgTransformModel.log ) 
        return query
    
    def deleteData(self):
        try:
            #self.engine.execute(sa_text('''TRUNCATE TABLE data''').execution_options(autocommit=True))
            session = self.Session()
            session.execute("TRUNCATE TABLE data;")
            session.commit()
            session.close()
            return True
        except:
            print("error: "+str(e))
            return False
#Creamos un obejto del tipo "OperationsBD" para comenzar el chequeo respectivo desde su función __init__
check_bd = OperationsBD()

#Se declaran lo modelos que crearan las tablas en la bd en caso de no existir.
class ImgTransformModel(check_bd.Base):
    __tablename__ = 'data'

    id = Column(Integer, primary_key=True)
    name = Column(Text)
    operation = Column(Text)
    time = Column(Text)
    success = Column(Boolean)
    log = Column(Text)

#Se crean las tablas previamente declaradas.
#check_bd.create_tables()
#check_bd.insertData()
############UBICAR EN UN PY APARTE
############UBICAR EN UN PY APARTE
############UBICAR EN UN PY APARTE