//alert("Cargado functions.js");

$('#beginbtn').click(function(){
    
    var principal_form = new FormData();
    var media = document.getElementById('imagen');

    principal_form.append('procedure', $('#optselect').val());
    principal_form.append('pictureFile', media.files[0]);

    $.ajax({
        url: '/setfilter/',
        type: 'POST',
        data: principal_form,
        dataType: "html",
        cache: false,// Subir archivo sin necesidad de cachar
        processData: false,// se utiliza para serializar los parámetros de datos, esto debe ser falso
        contentType: false, //deber
        success:function(data){
            $('#img_results').html(data);
        },
        error:function(){ 
            alert('Error en proceso.');								
        }
    });
});

$('#statsbtn').click(function(){
    
    var principal_form = new FormData();

    principal_form.append('regcantidad', $('#cantregs').val());

    $.ajax({
        url: '/getdata/',
        type: 'POST',
        data: principal_form,
        dataType: "html",
        cache: false,// Subir archivo sin necesidad de cachar
        processData: false,// se utiliza para serializar los parámetros de datos, esto debe ser falso
        contentType: false, //deber
        success:function(data){
            $('#data_res').html(data);
        },
        error:function(){ 
            alert('Error en proceso.');								
        }
    });
});

$('#deletebtn').click(function(){

    $.ajax({
        url: '/deletedata/',
        type: 'POST',
        success:function(data){
            $('#data_res').html(data);
        },
        error:function(){ 
            alert('Error en proceso.');								
        }
    });
});